package fr.epsi.adresse;

import fr.epsi.adhesion.Adhesion;
import fr.epsi.adhesion.AdhesionRepository;
import fr.epsi.adhesion.ValidationException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by gabriel on 29/06/17.
 */
@WebServlet("/address")
public class AdresseServlet extends HttpServlet {

    @EJB
    private AdresseRepository adresseRepository;

    @EJB
    private AdhesionRepository adhesionRepository;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Adhesion adhesion = adhesionRepository.get(Integer.parseInt(req.getParameter("id")));
            req.setCharacterEncoding("UTF-8");
            req.setAttribute("adresses", adhesion.getAdresses());
            getServletContext().getRequestDispatcher("/WEB-INF/adresse/listWithForm.jsp").forward(req, resp);
        } catch (SQLException e) {
            log("Problème lors de la récupération de la liste d'adresses d'un adhérent", e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int adhesionId = Integer.parseInt(req.getParameter("adhesionID"));
        Adresse adresse = new Adresse(
                req.getParameter("rue"),
                req.getParameter("codePostal"),
                req.getParameter("ville"),
                adhesionId
        );
        try {
            adresseRepository.create(adresse);
            getServletContext().getRequestDispatcher("/WEB-INF/adresse/succes.jsp").forward(req, resp);
        } catch(ValidationException e) {
            Map<String, Boolean> mapRaisons = e.getRaisons().stream().collect(Collectors.toMap(Function.identity(), x->true));
            req.setAttribute("raisons", mapRaisons);
            req.setAttribute("adresse", adresse);
            doGet(req, resp);
        } catch(SQLException e) {
            log("Problème lors de la création de l'adresse", e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
