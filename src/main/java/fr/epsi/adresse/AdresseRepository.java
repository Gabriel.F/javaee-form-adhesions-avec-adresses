package fr.epsi.adresse;

import fr.epsi.adhesion.ValidationException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.SQLException;

/**
 * Created by gabriel on 29/06/17.
 */
@Stateless
public class AdresseRepository {

    @PersistenceContext(unitName="adherentPersistenceUnit")
    private EntityManager entityManager;

    public void create(Adresse adresse) throws SQLException, ValidationException {
        adresse.valider();
        entityManager.persist(adresse);
    }
}
