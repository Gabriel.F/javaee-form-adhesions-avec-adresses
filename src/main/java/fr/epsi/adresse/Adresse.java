package fr.epsi.adresse;

import fr.epsi.adhesion.ValidationException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gabriel on 29/06/17.
 */
@Entity
@Table(name="Adresse")
public class Adresse {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(length = 200)
    private String rue;

    @Basic
    @Column(length = 20)
    private String codePostal;

    @Basic
    @Column(length = 200)
    private String ville;

    @Basic
    private int adhesionId;

    public Adresse() {}

    public Adresse(String rue, String codePostal, String ville, int adhesionId) {
        this.rue = rue;
        this.codePostal = codePostal;
        this.ville = ville;
        this.adhesionId = adhesionId;
    }

    /**
     * Basic verification
     * @throws ValidationException throws an exception if there is at least 1 error
     */
    public void valider() throws ValidationException {
        List<String> raisons = new ArrayList<>();

        if (this.rue == null || "".equals(this.rue)) {
            raisons.add("adresse.rue.vide");
        }

        if (this.codePostal == null || "".equals(this.codePostal)) {
            raisons.add("adresse.codePostal.vide");
        }
        else if(this.codePostal.length() != 5) {
            raisons.add("adresse.codePostal.longueurIncorrecte");
        }

        if (this.ville == null || "".equals(this.ville)) {
            raisons.add("adresse.ville.vide");
        }

        if (!raisons.isEmpty()) {
            throw new ValidationException(raisons);
        }
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
