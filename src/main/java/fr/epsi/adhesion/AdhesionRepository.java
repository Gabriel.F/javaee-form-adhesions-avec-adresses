package fr.epsi.adhesion;

import fr.epsi.api.APIAdhesion;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.SQLException;
import java.util.List;

@Stateless
public class AdhesionRepository {

	@PersistenceContext(unitName="adherentPersistenceUnit")
	private EntityManager entityManager;
	
	public void create(Adhesion adhesion, String confirmationMotDePasse) throws SQLException, ValidationException {
		adhesion.valider(confirmationMotDePasse);
        entityManager.persist(adhesion);
	}

	public void create(APIAdhesion jsonAdhesion) throws SQLException, ValidationException {
	    Adhesion adhesion = new Adhesion();
	    adhesion.setEmail(jsonAdhesion.getEmail());
	    adhesion.setMotDePasse(jsonAdhesion.getMotDePasse());
	    adhesion.setConditionsAcceptees(true);
	    create(adhesion, adhesion.getMotDePasse());
    }
	
	public List<Adhesion> getAll() throws SQLException {
		return entityManager.createQuery("select a from Adhesion a", Adhesion.class)
                .getResultList();
	}

	public Adhesion get(int id) throws SQLException {
		return entityManager.createQuery("select a from Adhesion a where a.id = :id", Adhesion.class)
				.setParameter("id", id)
				.getSingleResult();
	}

	public void delete(APIAdhesion adhesion) throws SQLException {
	    entityManager.createQuery("delete from Adhesion a where a.email = :email")
                .setParameter("email", adhesion.getEmail())
                .executeUpdate();
	}
}
