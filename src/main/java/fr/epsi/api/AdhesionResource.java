package fr.epsi.api;

import fr.epsi.adhesion.Adhesion;
import fr.epsi.adhesion.AdhesionRepository;
import fr.epsi.adhesion.ValidationException;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by gabriel on 31/05/17.
 */
@Path("/api/adhesion")
public class AdhesionResource {

    @EJB
    private AdhesionRepository adhesionRepository;

    @GET
    @Path("/get")
    public Response get() {
        Response resp = null;
        ArrayList<APIAdhesion> adhesions = new ArrayList<>();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            for(Adhesion adhesion : adhesionRepository.getAll()) {
                adhesions.add(new APIAdhesion(adhesion.getEmail(), formatter.format(adhesion.getDateAdhesion())));
            }
            resp = Response.ok(adhesions).build();
        }
        catch(SQLException e) {
            resp = Response.status(Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.TEXT_PLAIN_TYPE)
                    .entity("Error : a problem occured with the database")
                    .build();
        }
        return resp;
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(APIAdhesion adhesion) {
        Response resp = null;
        try {
            adhesionRepository.create(adhesion);
            resp = Response.status(Status.CREATED)
                    .type(MediaType.TEXT_PLAIN_TYPE)
                    .entity("Success : user created successfully")
                    .build();
        }
        catch(SQLException e) {
            resp = Response.status(Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.TEXT_PLAIN_TYPE)
                    .entity("Error : a problem occured with the database")
                    .build();
        }
        catch(ValidationException e) {
            resp = Response.status(Status.BAD_REQUEST)
                    .type(MediaType.TEXT_PLAIN_TYPE)
                    .entity("Error : email and/or password are incorrect")
                    .build();
        }
        return resp;
    }

    @POST
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(APIAdhesion adhesion) {
        Response resp = null;
        try {
            adhesionRepository.delete(adhesion);
            resp = Response.status(Status.OK)
                    .type(MediaType.TEXT_PLAIN_TYPE)
                    .entity("Success : user deleted successfully")
                    .build();
        }
        catch(SQLException e) {
            resp = Response.status(Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.TEXT_PLAIN_TYPE)
                    .entity("Error : a problem occured with the database")
                    .build();
        }
        return resp;
    }
}

