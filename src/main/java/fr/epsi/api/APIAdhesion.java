package fr.epsi.api;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by gabriel on 31/05/17.
 */
@XmlRootElement
public class APIAdhesion {

    @XmlElement(name="email")
    private String email;

    private String dateAdhesion;

    @XmlElement(name="motDePasse")
    private String motDePasse;

    public APIAdhesion() {}

    public APIAdhesion(String email, String dateAdhesion) {
        this.email = email;
        this.dateAdhesion = dateAdhesion;
    }

    public String getEmail() {
        return email;
    }

    public String getMotDePasse() {
        return motDePasse;
    }
}
