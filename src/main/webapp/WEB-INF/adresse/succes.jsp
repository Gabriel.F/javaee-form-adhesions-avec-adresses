<%@page pageEncoding="UTF-8" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Adresse</title>
</head>
<body>
	<p>L'ajout d'adresse a bien été pris en compte</p>
	<p><a href="<c:url value="/index.jsp"/>">Retour à l'accueil</a></p>
	<p><a href="<c:url value="/adhesion"/>">Formulaire d'adhésion</a></p>
  	<p><a href="<c:url value="/adhesions"/>">Voir la liste des adhésions</a></p>
</body>
</html>
