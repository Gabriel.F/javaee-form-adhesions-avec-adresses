<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8" />
    <link type="text/css" rel="stylesheet" href="<c:url value="/css/style.css"/>" />
    <title>Liste d'adresses</title>
</head>
<body>

    <c:choose>
        <c:when test="${empty adresses}">
            <p>Aucune adresse n'a encore été ajoutée</p>
        </c:when>
        <c:otherwise>
            <table>
                <caption>Liste des adresses</caption>
                <tbody>
                <c:forEach var="a" items="${adresses}">
                    <tr>
                        <td><c:out value="${a.rue}"/></td>
                        <td><c:out value="${a.codePostal}"/></td>
                        <td><c:out value="${a.ville}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:otherwise>
    </c:choose>

    <form method="post">
        <h1>Ajout d'une adresse</h1>

        <p>
            <label for="rue">Rue</label>
            <input type="text" id="rue" name="rue" value="<c:out value="${adresse.rue}"/>" />
            <c:if test="${raisons['adresse.rue.vide']}">
                <p class="error">Vous devez saisir votre rue&nbsp;!</p>
            </c:if>
        </p>
        <p>
            <label for="codePostal">Code postal</label>
            <input type="text" id="codePostal" name="codePostal" value="<c:out value="${adresse.codePostal}"/>" />
            <c:if test="${raisons['adresse.codePostal.vide']}">
                <p class="error">Vous devez saisir votre code postal&nbsp;!</p>
            </c:if>
            <c:if test="${raisons['adresse.codePostal.longueurIncorrecte']}">
                <p class="error">Le code postal ne doit faire que 5 chiffres&nbsp;!</p>
            </c:if>
        </p>
        <p>
            <label for="ville">Ville</label>
            <input type="text" id="ville" name="ville" value="<c:out value="${adresse.ville}"/>" />
            <c:if test="${raisons['adresse.ville.vide']}">
                <p class="error">Vous devez saisir votre ville&nbsp;!</p>
            </c:if>
        </p>

        <input type="hidden" name="adhesionID" value="${param.id}" />

        <p><button type=submit>Valider</button></p>
    </form>

    <p><a href="<c:url value="/index.jsp"/>">Retour à l'accueil</a></p>
</body>
</html>
