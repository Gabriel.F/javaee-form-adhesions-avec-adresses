# En résumé

Reprise du projet avec les adhésions & Jax RS pour rajouter la possibilité :
* d'ajouter une adresse à une adhésion
* de lister toutes les adresses d'une adhésion

Le tout avec JPA, et en n'oubliant pas de faire une vérification basique du formulaire de l'adresse.